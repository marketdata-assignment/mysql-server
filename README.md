README documentation about How to launch the application

1. You need to have some applications install on your computer

-	Git 				https://git-scm.com/Downloads 		lone project sources.
-	Nodejs with npm		https://nodejs.org/en/				install angular dependencies.
-	Angular cli 		npm install -g @angular/cli 		Serve front end application.
-	RDBMS (MySql Server & MySql Workbench)				
https://dev.mysql.com/get/Downloads/MySQLInstaller/mysql-installer-web-community-5.7.21.0.msi
															Create database and ressources.
-	Visual studio       https://www.visualstudio.com/fr/	Run C# Rest API.

2. You need to create market data database

With MySql Server or any other RDBMS

Run your MySql Server and execute the following queries to create database and ressources

create schema market;
create table market.stock_prices
(
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    brand VARCHAR(100),
    date_stock DATETIME,
    open_price FLOAT,
    high FLOAT,
    low FLOAT,
    close_price FLOAT
);
create user 'ALEXANDRE' identified by 'password';
grant all privileges on market.stock_prices to 'ALEXANDRE';

3. You need to get back project sources on your computer

-	Open https://gitlab.com/marketdata-assignment/ on your web browser
-	Clone angular client https://gitlab.com/marketdata-assignment/angular-client.git
-	Clone api-market-aspnet https://gitlab.com/marketdata-assignment/api-market-aspnet.git

4. You need to Run api-market-aspnet application

-	Open terminal console
-	Go to api-market-aspnet project directory
-	run "start api-market-aspnet.sln" to open the project
-	Start the C# Rest API application

5. You need to serve angular-client application

-	Open terminal console
-	Go to angular-client project directory
-	run "npm install" command to recover all dependencies
-	run "npm install -g @angular/cli" if angular-cli is not installed yet
-	run "ng serve --open" to start front-ent application

6. You Can now try the application

-	From the interface you have thre tables, two for market data and an other ones for calculation
-	Import AAPL.csv file from (angular-client/assets) and click Import CSV button
-	Import S&P500.csv file from (angular-client/assets) and click Import CSV button
-	You can click on Calculate Statistics button to have delta between prices